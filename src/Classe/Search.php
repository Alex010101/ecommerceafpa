<?php

namespace App\Classe;

use App\Entity\Category;

class Search
{
    /**
     * Création d'une nouvelle classe permettant de représenter la recherche effectuer par l'utilisateur sous forme d'objet.
     * @var string
     */
    public $string = '';

    /**
     * @var Category[]
     */
    public $categories = [];
    
}

