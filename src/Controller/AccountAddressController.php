<?php

namespace App\Controller;

use App\Classe\Cart;
use App\Entity\Address;
use App\Form\AddressType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AccountAddressController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $em)
    {
        $this->entityManager = $em;
    } 
    
    #[Route('/compte/adresses', name: 'account_address')]
    public function index(): Response
    {
        return $this->render('account/address.html.twig');
    }

    // AJOUTER UNE ADRESSE
    #[Route('/compte/ajouter-une-adresse', name: 'account_address_add')]
    public function add(Cart $cart, Request $request): Response
    {   
        // instanciation de l'objet adresse
        $address = new Address();
        // récupération de la requete
        $form = $this->createForm(AddressType:: class, $address );
        // la requete est envoyéé
        $form->handleRequest($request);
        // si le formulaire est soumit et validé je récupére les infos sais par l'utilisateur 
        // elles seront persistées et envoyées en BDD
            if($form->isSubmitted() && $form->isValid()) {
                $address->setUser($this->getUser());
                $this->entityManager->persist($address);
                $this->entityManager->flush();
                if ($cart->get()) {
                        //$this->addFlash('success', 'Votre adresse a bien été ajoutée');
                        return $this->redirectToRoute('account_address');
                } else {
                        //$this->addFlash('danger', "Votre adresse n'a pas été créée");
                        return $this->redirectToRoute('account_address');
                }
            }
        
        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    // MODIFIER UNE ADRESSE
    #[Route('/compte/modifier-une-adresse/{id<\d+>}', name: 'account_address_edit')]
    public function edit(Request $request, $id): Response
    {
        $address = $this->entityManager->getRepository(Address::class)->findOneById($id);

        // si l'adresse n'existe pas ou n'appartient pas à un utilisateur on renvoit vers la page pour créer une adresse
        if (!$address || $address->getUser() != $this->getUser()) {
            return $this->redirectToRoute('account_address');
        }
            $form = $this->createForm(AddressType::class, $address);

            $form->handleRequest($request);

        // si le formulaire est soumit et validé on renvoit vers la page ou apparaissent les adresses
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            //$this->addFlash('success', 'Votre adresse a bien été modifiée');
            return $this->redirectToRoute('account_address');
        }

        //formulaire de rensignements d'adresse
        return $this->render('account/address_form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    //SUPPRIMER UNE ADRESSE
    #[Route('/compte/supprimer-une-adresse/{id<\d+>}', name: 'account_address_delete')]
    public function delete($id): Response
    {
        $address = $this->entityManager->getRepository(Address::class)->findOneById($id);

        if ($address && $address->getUser() == $this->getUser()) {
            $this->entityManager->remove($address);
            $this->entityManager->flush();
            //$this->addFlash('success', 'Votre adresse a bien été supprimée');
        }

        return $this->redirectToRoute('account_address');
    }

}
