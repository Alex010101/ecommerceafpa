<?php

namespace App\Controller\Admin;

use App\Entity\Parure;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class ParureCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Parure::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),    
        ];
    }
    
}
