<?php

namespace App\Controller;

use App\Classe\Cart;
use Doctrine\ORM\Query\AST\Functions\LengthFunction;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Validator\Constraints\Length;

class CartController extends AbstractController
{
    #[Route('/mon-panier', name: 'cart')]
    public function index(Cart $cart): Response
    {

        // Les calculs seraient ici
        $cart = $cart->getFull();

        // cette commande length m'enerve !!!!!
        // dd($cart->length);
        // dd($cart|length);
        // dd($cart.length);
        // dd($cart length);
        // dd(length($cart));

        // $truc = ($cart).Length;

        return $this->render('cart/index.html.twig', [
            // 'cart' => $cart->getFull()
            // 'truc' => $truc,
            'cart' => $cart
        ]);
    }

    #[Route('/cart/add/{id}', name: 'add_to_cart')]
    public function add(Cart $cart, $id): Response
    {
        $cart->add($id);
        return $this->redirectToRoute('cart');
    }

    #[Route('/cart/remove', name: 'remove_my_cart')]
    public function remove(Cart $cart): Response
    {
        $cart->remove();
        return $this->redirectToRoute('home');
    }

    #[Route('/cart/delete/{id}', name: 'delete_to_cart')]
    public function delete(Cart $cart, $id): Response
    {
        $cart->delete($id);

        return $this->redirectToRoute('cart');
    }

    #[Route('/cart/decrease/{id}', name: 'decrease_to_cart')]
    public function decrease(Cart $cart, $id): Response
    {
        $cart->decrease($id);

        return $this->redirectToRoute('cart');
    }
}
