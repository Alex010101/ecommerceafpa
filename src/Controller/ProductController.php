<?php

namespace App\Controller;



use App\Classe\Search;

use App\Entity\Product;
use App\Form\SearchType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProductController extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    #[Route('/nos-produits', name: 'products')]
        public function index(Request $request): Response
    {
        // initialisation de la classe Search qui sera passer à la méthode create
        $search = new Search;
         $form = $this->createForm(SearchType::class, $search);
        // handleRequest qui va écouter le formulaire
        $form->handleRequest($request);
        // Si le formulaire est soumis et valide, d'effectuer la rechercher en appelant le repository de product 
        // avec une nouvelle fonction findWithSearch
        if ($form->isSubmitted() && $form->isValid()) {
            $products = $this->entityManager->getRepository(Product::class)->findWithSearch($search);
        } else {
            $products = $this->entityManager->getRepository(Product::class)->findAll();  
        }
        return $this->render('product/index.html.twig', [
            'products' => $products,
            'form' => $form->createView()
        ]);
    }

    #[Route('/produit/{slug}', name: 'product')]
    public function show($slug): Response
    {
        $product = $this->entityManager->getRepository(Product::class)->findOneBySlug($slug);
        
        if (!$product) {

            return $this->redirectToRoute('product');
        }
        
        return $this->render('product/show.html.twig', [
            'product' => $product
    
        ]);
    }
}
