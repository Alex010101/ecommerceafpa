<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class RegisterController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    #[Route('/inscription', name: 'register')]
    public function index(Request $request, UserPasswordHasherInterface $hasher): Response
    {
        $notification = null;
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        /*
        Si le formulaire est valide, rappelle l'objet User instancié plus haut,
        dire à l'objet User d'injecter dans l'objet User toutes les donner qu'il récupère du formulaire
        ensuite tester avec un dd pour voir le résultats.
        */
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            /**
             * Vérifer que l'utilisateur n'est pas déjà existant en base
             */
            $search_email = $this->entityManager->getRepository(User::class)->findOneByEmail($user->getEmail());

            if (!$search_email) {
                /**
                 * On peut se servir de l'orm Doctrine pour persister les données en base, 
                 * en utilisant la convention on utilisera le manager de Doctrine entityManager qui sera la varaible pour appeler 
                 * Doctrine,
                 * donc on va persister l'entité $user, ensuite utiliser la méthode flush pour exécuter la persistance.
                 * Il prend la data donc l'objet et l'enrengistre en base.
                 */
                $password = $hasher->hashPassword($user, $user->getPassword());
                // dd($user);
                // dd($password);
                /**
                 * On va stocker dans le variable $password le mot de passe de l'utilisateur 
                 */
                $user->setPassword($password);
                $this->entityManager->persist($user);
                $this->entityManager->flush();
                $notification = "Votre inscription à bien été pris en compte.";

            } else {
                $notification = "Votre inscription n'a pas été pris en compte ou votre email existe déjà.";
            }
        }
        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
            'notification' => $notification
        ]);
    }
}
