<?php

namespace App\Repository;

use App\Classe\Search;
use App\Entity\Product;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function save(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Product $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    /**
     * @param Search $search
     * Requête qui permet de récupérer les produits en fonction de la recherche l'utilisateur
     * grâce à Doctrine et outils fournis.
     * @return Product[]
     */
    public function FindWithSearch(Search $search)
    {   
        /**
         * Création de la variable $query puis de createQueryBuilder méthode importante pour la création
         * de la requête ensuite select en utilisant les alias (p) pour Product, (c) pour Category pour faire 
         * jointure entre les produits et les catégories.
         */ 
        $query = $this
            ->createQueryBuilder('p')
            ->select('c','p')
            ->join('p.category', 'c');
        
        // si l'utilisateur renseigne des catégories à rechercher, il exécute la requête
        // pour lui dire à quoi correspond la variable j'ajoute la méthode setParameter avec le nom de la variable
        //donc 'categories' et deuxième paramètre la valeur de clé
        if(!empty($search->categories)) {
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $search->categories);
        }

        // si l'utilisateur renseigne une recherche textuelle, il exécute la requête
        if (!empty($search->string)) {
            $query = $query
            ->andWhere('p.name LIKE :string')
            ->setParameter('string', "%{$search->string}%");
        }
        return $query->getQuery()->getResult();
    }


//    /**
//     * @return Product[] Returns an array of Product objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('p.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Product
//    {
//        return $this->createQueryBuilder('p')
//            ->andWhere('p.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
